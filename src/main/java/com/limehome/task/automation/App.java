package com.limehome.task.automation;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
	

	public static void main(String[] args) throws InterruptedException {

		ChromeDriver driver = null;
		
		try {
			String path = System.getProperty("user.dir");
			System.out.println(path);
			System.setProperty("webdriver.chrome.driver", path + "\\source\\chromedriver.exe");
			 driver = new ChromeDriver();
			driver.get("https://limehome-qa-task.herokuapp.com/");
			Thread.sleep(6000);
			WebElement name = driver.findElement(By.id("mat-input-0"));
			name.sendKeys("Devanesan");
			String fName = name.getAttribute("value");
			WebElement reference = driver.findElement(By.id("mat-input-1"));
			reference.sendKeys("ABCDEFG");
			driver.findElementByXPath(".//*[text()='Submit']").click();
			Thread.sleep(5000);
			String Lname = driver.findElement(By.id("mat-input-3")).getAttribute("value");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,250)");
			System.out.println("Last Name entered in Check-in Screen :\n" + Lname);
			System.out.println("Auto Populated Last Name:\n" + fName);
			if (fName.equals(Lname)) {
				System.out.println("Test Case Passed: Check-in Last Name Auto populated in Retrieved form screen\n");
			
			} else
				System.out.println(" Test Case faile: Entered Last Name Auto populated in Retrieve form screen failed\n");
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}finally {
			driver.close();
		}
	}

}